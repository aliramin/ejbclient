package com.tsn;

import com.tsn.stateless.LibrarySessionBean;
import com.tsn.stateless.LibrarySessionBeanRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

public class Client {

    public static void main(String[] args) {

        try {

            final Context context = getInitialContext();

            //LibrarySessionBeanRemote beanRemote = (LibrarySessionBeanRemote) context.lookup("LibrarySessionBeanRemote#com.tsn.stateless.LibrarySessionBeanRemote");
            LibrarySessionBeanRemote beanRemote = (LibrarySessionBeanRemote) context.lookup("LibrarySessionBean#com.tsn.stateless.LibrarySessionBean");
            //LibrarySessionBeanRemote beanRemote = (LibrarySessionBeanRemote) context.lookup("java:comp/env/ejb/LibrarySessionBean");
            //LibrarySessionBeanRemote beanRemote = (LibrarySessionBeanRemote) context.lookup(" java:comp/env/ejb/MyServiceBean/remote");

            beanRemote.sayHello("Ali");

        } catch (NamingException e) {
            e.printStackTrace();
        }

    }

    public static Context getInitialContext() throws NamingException {

        Hashtable env = new Hashtable();

        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL , "t3://localhost:7002");
        return new InitialContext(env);
    }




//    BufferedReader brConsoleReader = null;
//    Properties props;
//    Context ctx;
//
//    {
//
//        Hashtable ht = new Hashtable();
//        ht.put(Context.INITIAL_CONTEXT_FACTORY,
//                "weblogic.jndi.WLInitialContextFactory");
//        ht.put(Context.PROVIDER_URL,
//                "t3://localhost:7001");
//
//        try {
//            ctx = new InitialContext(ht);
//            // Use the context in your program
//        } catch (NamingException e) {
//            // a failure occurred
//        }
////        finally {
////            try {ctx.close();}
////            catch (Exception e) {
////                // a failure occurred
////            }
////        }
//
//
////        props = new Properties();
////        try {
////            props.load(new FileInputStream("jndi.properties"));
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        }
//
////        try {
////            ctx = new InitialContext(props);
////        } catch (NamingException ex) {
////            ex.printStackTrace();
////        }
//
//
//        /////////////////////////////////////////////
//
////        try {
////            ctx = new InitialContext();
////        } catch (NamingException e) {
////            e.printStackTrace();
////        }
//
//        ///////////////////////////////////////////////
//
////        Hashtable env = new Hashtable(5);
////        env.put(Context.INITIAL_CONTEXT_FACTORY,
////                "weblogic.jndi.WLInitialContextFactory");
////        env.put(Context.PROVIDER_URL,
////                "t3://weblogicServer:7001");
////
////        try {
////            ctx = new InitialContext(env);
////        } catch (NamingException e) {
////            e.printStackTrace();
////        }
//
//        ///////////////////////////////////////////////
//
//        brConsoleReader =
//                new BufferedReader(new InputStreamReader(System.in));
//    }
//
//    public static void main(String[] args) {
//
//        Client ejbTester = new Client();
//
//        ejbTester.testStatelessEjb();
//    }
//
//    private void showGUI() {
//        System.out.println("**********************");
//        System.out.println("Welcome to Book Store");
//        System.out.println("**********************");
//        System.out.print("Options \n1. Add Book\n2. Exit \nEnter Choice: ");
//    }
//
//    private void testStatelessEjb() {
//        try {
//
//            int choice = 1;
//            LibrarySessionBeanRemote libraryBean =
//                    (LibrarySessionBeanRemote) ctx.lookup("LibrarySessionBean/remote");
//            while (choice != 2) {
//                String bookName;
//                showGUI();
//                String strChoice = brConsoleReader.readLine();
//                choice = Integer.parseInt(strChoice);
//                if (choice == 1) {
//                    System.out.print("Enter book name: ");
//                    bookName = brConsoleReader.readLine();
//                    libraryBean.addBook(bookName);
//                } else if (choice == 2) {
//                    break;
//                }
//            }
//            List<String> booksList = libraryBean.getBooks();
//            System.out.println("Book(s) entered so far: " + booksList.size());
//            for (int i = 0; i < booksList.size(); ++i) {
//                System.out.println((i + 1) + ". " + booksList.get(i));
//            }
//            LibrarySessionBeanRemote libraryBean1 =
//                    (LibrarySessionBeanRemote) ctx.lookup("LibrarySessionBean/remote");
//            List<String> booksList1 = libraryBean1.getBooks();
//            System.out.println(
//                    "***Using second lookup to get library stateless object***");
//            System.out.println(
//                    "Book(s) entered so far: " + booksList1.size());
//            for (int i = 0; i < booksList1.size(); ++i) {
//                System.out.println((i + 1) + ". " + booksList1.get(i));
//            }
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        } finally {
//            try {
//                if (brConsoleReader != null) {
//                    brConsoleReader.close();
//                }
//            } catch (IOException ex) {
//                System.out.println(ex.getMessage());
//            }
//        }
//    }
}
